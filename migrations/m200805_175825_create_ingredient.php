<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200805_175825_create_ingredient
 */
class m200805_175825_create_ingredient extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ingredient}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
            'hidden'=>Schema::TYPE_BOOLEAN . ' NOT NULL'
        ], $tableOptions);

        $this->insert('{{%ingredient}}', [
           'id'=>1,
           'name'=>'chicken',
           'hidden'=>true
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>2,
           'name'=>'beef',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>3,
           'name'=>'egg',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>4,
           'name'=>'milk',
           'hidden'=>true
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>5,
           'name'=>'cheese',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>6,
           'name'=>'tomato',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>7,
           'name'=>'cucumber',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>8,
           'name'=>'potato',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>9,
           'name'=>'dill',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>10,
           'name'=>'orange',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>11,
           'name'=>'apple',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>12,
           'name'=>'oatmeal',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>13,
           'name'=>'butter',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>14,
           'name'=>'peach',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>15,
           'name'=>'apricot',
           'hidden'=>false
        ]);
        $this->insert('{{%ingredient}}', [
           'id'=>16,
           'name'=>'mayo',
           'hidden'=>false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('ingredient');
    }
}
