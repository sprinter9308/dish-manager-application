<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m200805_175724_create_dish
 */
class m200805_175724_create_dish extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dish}}', [
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->insert('{{%dish}}', [
           'id'=>1,
           'name'=>'Vegetable salad' // Tomate Cucumber Potato
        ]);

        $this->insert('{{%dish}}', [
           'id'=>2,
           'name'=>'Quick lunch' // Beef Potato Butter Dill
        ]);

        $this->insert('{{%dish}}', [
           'id'=>3,
           'name'=>'Fruit dream' // Orange Milk Apple Peach Apricot
        ]);

        $this->insert('{{%dish}}', [
           'id'=>4,
           'name'=>'Simple vegetable salad' // Tomate Cucumber
        ]);

        $this->insert('{{%dish}}', [
           'id'=>5,
           'name'=>'Cheese set' // Cheese Beef Dill Egg Mayo
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('dish');
    }
}
