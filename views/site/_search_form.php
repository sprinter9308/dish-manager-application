<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'id' => 'search-model',
        'action'=>'/index',
        'options'=>[
            'class'=>'search-form'
        ]
    ]); ?>

    <?= $form->field($search_model, 'ingredient_one', ['enableLabel'=>false, 'options'=>['class'=>'search-form-field']])->textInput(['class'=>'search-form-field-input', 'placeholder'=>'First']) ?>
    <?= $form->field($search_model, 'ingredient_two', ['enableLabel'=>false, 'options'=>['class'=>'search-form-field']])->textInput(['class'=>'search-form-field-input', 'placeholder'=>'Second']) ?>
    <?= $form->field($search_model, 'ingredient_three', ['enableLabel'=>false, 'options'=>['class'=>'search-form-field']])->textInput(['class'=>'search-form-field-input', 'placeholder'=>'Third']) ?>
    <?= $form->field($search_model, 'ingredient_four', ['enableLabel'=>false, 'options'=>['class'=>'search-form-field']])->textInput(['class'=>'search-form-field-input', 'placeholder'=>'Four']) ?>
    <?= $form->field($search_model, 'ingredient_five', ['enableLabel'=>false, 'options'=>['class'=>'search-form-field']])->textInput(['class'=>'search-form-field-input', 'placeholder'=>'Five']) ?>

    <?php if (!empty($search_message)) echo '<div class="index-message-block">'.$search_message.'</div>'; ?>

    <?= Html::submitButton('Искать', ['class' => 'search-form-button', 'name' => 'login-button']) ?>
<?php ActiveForm::end(); ?>
