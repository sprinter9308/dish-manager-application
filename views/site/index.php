<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Поиск блюд';
?>

<?= $this->render('_search_form', [
        'search_model'=>$search_model,
        'search_message'=>$search_message
    ]); ?>

<?= $this->render('_search_results', [
        'search_data'=>$search_data
    ]); ?>
