<table class="index-search-results">
<?php foreach($search_data as $dish): ?>
    <tr>
        <td><?= $dish['name'] ?></td>
        <td class="ingredient-list">(<?= $dish['ingredients'] ?>)</td>
    </tr>
<?php endforeach; ?>
</table>
