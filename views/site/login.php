<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход в систему';
?>
<div class="login-page">

    <?= $this->render('_login_form', [
        'model'=>$model,
    ]); ?>
</div>