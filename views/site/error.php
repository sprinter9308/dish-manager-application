<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = "Ошибка";
?>
<div class="site-error">

    <h3>Возникла ошибка, вернитесь на главную страницу</h3>

    <?= Html::a('Вернуться на главную', '/', ['class' => 'standart-button dish-manage-button']) ?>
</div>
