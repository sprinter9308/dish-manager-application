<?php
//use app\helpers\UrlTransform;
use yii\helpers\Html;
//use yii\grid\GridView;
$this->title = 'Административный раздел';
?>

<?php if (!empty($message)): ?>
    <div class="admin-message">
        <?= $message ?>
    </div>
<?php endif; ?>

<div class="admin-main-container">
    <div class="admin-dish-list">
        <h3>Блюда</h3>

        <?= $this->render('_dish_list', [
            'dish_index_data_provider'=>$dish_index_data_provider
        ]); ?>

        <?= Html::a('Добавить блюдо', '/dish/create', ['class'=>'standart-button admin-dish-add-button']) ?>
    </div>
    <div class="admin-ingredient-list">
        <h3>Ингредиенты</h3>
        <?= $this->render('_ingredient_list', [
            'ingredient_index_data_provider'=>$ingredient_index_data_provider
        ]); ?>

        <?= Html::a('Добавить ингредиент', '/ingredient/create', ['class'=>'standart-button admin-ingredient-add-button']) ?>
    </div>
</div>
