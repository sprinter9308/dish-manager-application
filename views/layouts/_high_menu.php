<?php
use yii\helpers\Html;
?>

<?php if (Yii::$app->request->url !== '/' && Yii::$app->request->url !== '/index'): ?>
    <?= Html::a('Главная', '/', ['class'=>'high-panel-main-page']) ?>
<?php endif; ?>

<?php if (Yii::$app->user->isGuest): ?>
    <?php
        if (Yii::$app->request->url !== '/login') {
            echo Html::a('Войти', '/login', ['class'=>'high-panel-login']);
        };
    ?>
<?php else: ?>
        <?php
            if (strpos(Yii::$app->request->url, '/admin')!==0) {
                echo Html::a('Админка', '/admin', ['class'=>'high-panel-admin']);
            }
        ?>
    <?= Html::a('Выйти', '/logout', ['class'=>'high-panel-logout']) ?>
<?php endif; ?>
