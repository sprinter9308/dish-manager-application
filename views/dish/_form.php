<?php
//use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'id' => 'dish-manage-model',
        'options'=>[
        ]
    ]); ?>

    <?= $form->field($dish_model, 'name', ['options'=>['label'=>'Название', 'class'=>'dish-manage-form-field']])
            ->textInput(['class'=>'dish-manage-field-input', 'placeholder'=>'Введите название', 'value'=> $dish_model->name ?? Yii::$app->session->get('new_dish_name')])
            ->label('Название') ?>

    <?= $form->field($dish_model, 'choice_ingredient', ['options'=>['label'=>'Выберите ингредиент','class'=>'dish-manage-form-field']])
            ->listBox($available_ingredient_list, ['class'=>'dish-manage-field-list'])
            ->label('Выберите ингредиент') ?>

    <?= Html::submitButton($name_button_action, ['class' => 'standart-button dish-manage-button', 'name' => 'dish-manage-button']) ?>
<?php ActiveForm::end(); ?>
