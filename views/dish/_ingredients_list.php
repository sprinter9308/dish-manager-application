<?php
use yii\helpers\Html;
?>

<?php foreach ($manage_dish_ingredients_list as $manage_ingredient): ?>
    <div>
        <span><?= $manage_ingredient['name'] ?></span>
        <?= Html::a('&#10006;', $url_action_link.$manage_ingredient['index'], ['class' => 'dish-manage-delete-button']) ?>
    </div>
<?php endforeach; ?>
