<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Редактирование блюда '.$dish_model->name;
?>

<?php if (!empty($message)): ?>
    <div class="admin-message">
        <?= $message ?>
    </div>
<?php endif; ?>

<div class="dish-manage-container">

    <div clas="dish-manage-form">
        <?= $this->render('_form', [
            'dish_model'=>$dish_model,
            'name_button_action'=>'Добавить',
            'available_ingredient_list'=>$available_ingredient_list
        ]); ?>
    </div>

    <div class="dish-manage-ingredient-list">
        <?= $this->render('_ingredients_list', [
            'dish_model'=>$dish_model,
            'url_action_link'=>'/dish/update/'.$dish_model->id.'?discard=',
            'manage_dish_ingredients_list'=>$manage_dish_ingredients_list
        ]); ?>
    </div>

</div>
