<?php
$this->title = 'Редактирование ингредиента';
?>

<?= $this->render('_form', [
        'ingredient_model'=>$ingredient_model,
        'button_label'=>'Редактировать'
    ]); ?>
