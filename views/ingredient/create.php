<?php
$this->title = 'Добавление ингредиента';
?>

<?= $this->render('_form', [
        'ingredient_model'=>$ingredient_model,
        'button_label'=>'Добавить'
    ]); ?>
