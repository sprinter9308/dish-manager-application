<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'id' => 'ingredient-manage-model',
        'options'=>[
            'class'=>'ingredient-manage-form'
        ]
    ]); ?>

    <?= $form->field($ingredient_model, 'name', ['options'=>['class'=>'ingredient-manage-form-field']])->textInput(['class'=>'ingredient-manage-field-input', 'placeholder'=>'Введите название']) ?>
    <?= $form->field($ingredient_model, 'hidden', ['options'=>['class'=>'ingredient-manage-form-field']])->checkbox(['class'=>'ingredient-manage-field-checkbox']) ?>

    <?= Html::submitButton($button_label, ['class' => 'standart-button ingredient-manage-button', 'name' => 'ingredient-manage-button']) ?>
<?php ActiveForm::end(); ?>
