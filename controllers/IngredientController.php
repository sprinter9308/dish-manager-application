<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Ingredient;
use yii\web\NotFoundHttpException;

class IngredientController extends Controller
{
    private const HAVE_DISHES_FOR_DELETE_INGREDIENT = "Удаление ингредиента недоступно, имеются блюда где он задействован. Устраните его из них";

    private const INGREDIENT_SUCCESS_DELETE = "Ингредиент успешно удален";

    private const INGREDIENT_SUCCESS_CREATE = "Ингредиент успешно добавлен";

    private const INGREDIENT_SUCCESS_UPDATE = "Ингредиент успешно отредактирован";

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    public function actionCreate()
    {
        $ingredient_model = new Ingredient();
        return $this->manageAction($ingredient_model, [
            'view_name'=>'create',
            'success_message'=>static::INGREDIENT_SUCCESS_CREATE
        ]);
    }

    public function actionUpdate($id)
    {
        $ingredient_model = $this->findModel($id);
        return $this->manageAction($ingredient_model, [
            'view_name'=>'update',
            'success_message'=>static::INGREDIENT_SUCCESS_UPDATE
        ]);
    }

    public function actionDelete($id)
    {
        $linked_dishes = $this->findModel($id)->getDishes()->all();
        if (count($linked_dishes) > 0) {
            return $this->redirect(['/admin?message='.static::HAVE_DISHES_FOR_DELETE_INGREDIENT]);
        } else {
            $this->findModel($id)->delete();
            return $this->redirect(['/admin?message='.static::INGREDIENT_SUCCESS_DELETE]);
        }
    }

    private function manageAction($ingredient_model, $action_params)
    {
        if ($ingredient_model->load(Yii::$app->request->post()) && $ingredient_model->save()) {
            return $this->redirect(['/admin?message='.$action_params['success_message']]);
        } else {
            return $this->render($action_params['view_name'], [
                'ingredient_model' => $ingredient_model
            ]);
        }
    }

    private function findModel($id)
    {
        if (($ingredient_model = Ingredient::findOne($id)) !== null) {
            return $ingredient_model;
        } else {
            throw new NotFoundHttpException('Requested ingredient not exist');
        }
    }
}
