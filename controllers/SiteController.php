<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\SearchForm;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Dish;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        $dish = Dish::findOne(1);
//        $ingredients = $dish->getIngredients()->all();
//        $special = (new yii\db\Query())->select('di.dish_id, d.name, COUNT(di.ingredient_id) as quant')
//                                ->from('dish_ingredient di')
//                                ->where(['in', 'ingredient_id', [6,7,8,9,2,13]])
//                                ->groupBy(['di.dish_id'])
//                                ->having(['>', 'COUNT(di.ingredient_id)', 2])
//                                ->join('LEFT JOIN', 'dish d', 'd.id=di.dish_id')
//                                ->all();
//        echo '<pre>';
//        print_r($special);
//        echo '</pre>';
//        exit();


        $search_model = new SearchForm();

//        $find_result_model = [
//            ['one', 'two'],
//            ['three', 'four']
//        ];
//        $search_data = [
//            ['one', 'two'],
//            ['three', 'four']
//        ];

        $search_model->load(Yii::$app->request->post());
//        var_dump($search_model->ingredient_one);
//        var_dump($search_model->ingredient_one);

        if ($search_model->load(Yii::$app->request->post()) && $search_model->findSuitableDishes()) {

//            echo '<pre>';
//            print_r($search_model->getDishesList());
//            echo '</pre>';
//            exit();

            return $this->render('index', [
                'search_model' => $search_model,
                'search_data' => $search_model->getDishesList(),
                'search_message' => $search_model->getMessage()
            ]);

        } else {

            return $this->render('index', [
                'search_model' => $search_model,
    //            'search_data' => $search_data
                'search_data' => [],
                'search_message' => $search_model->getMessage()
    //            'catalog_index_data_provider' => $main_page_data_provider
            ]);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->redirect(Yii::$app->homeUrl);
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/admin']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
