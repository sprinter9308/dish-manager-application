<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Dish;
use app\models\Ingredient;

class DishController extends Controller
{
    private const NEW_DISH_SUCCESS_ADD = 'Новое блюдо успешно добавлено';

    private const NEW_DISH_ERROR_ADD = 'Произошла ошибка при добавлении нового блюда';

    private const DISH_SUCCESS_DELETE = 'Блюдо успешно удалено';

    private const UPDATE_DISH_WITHOUT_NAME = 'Ошибка при обновлении. У блюда должно быть название';

    private const TOO_FEW_INGREDIENTS_FOR_UPDATE_DISH = 'Ошибка при обновлении. У блюда остается слишком мало ингредиентов';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    public function actionCreate()
    {
        $dish_model = new Dish();

        $manage_dish_ingredients_list = Yii::$app->session->get('manage_dish_ingredients') ?? [];
        $message = Yii::$app->request->get()['message'] ?? null;

        if ($dish_model->load(Yii::$app->request->post())) {

            $manage_dish_ingredients_list = Dish::addNewInfoOnCreateProcess($dish_model, $manage_dish_ingredients_list);

            return $this->render('create', [
                'available_ingredient_list'=>Ingredient::getAvailableIngredientList($manage_dish_ingredients_list),
                'dish_model'=>$dish_model,
                'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($manage_dish_ingredients_list),
                'message'=>$message
            ]);

        } else {

            $manage_dish_ingredients_list = Dish::checkDiscardItemInIngredientList($manage_dish_ingredients_list);

            return $this->render('create', [
                'available_ingredient_list'=>Ingredient::getAvailableIngredientList($manage_dish_ingredients_list),
                'dish_model'=>$dish_model,
                'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($manage_dish_ingredients_list),
                'message'=>$message
            ]);
        }
    }



    public function actionUpdate($id)
    {
        $dish_model = $this->findDishModel($id);

        $update_dish_ingredients_list = $dish_model->getIngredientsIds();
        $message = Yii::$app->request->get()['message'] ?? null;

        if ($dish_model->load(Yii::$app->request->post())) {


            $add_results = Dish::checkAddIngredientToDish($dish_model, $update_dish_ingredients_list);

            if (!$add_results['success']) {
                return $this->redirect(['/dish/update/'.$id.'?message='.static::UPDATE_DISH_WITHOUT_NAME]);
            }

            $update_dish_ingredients_list = $add_results['modified_dish_ingredients_list'];

            return $this->render('update', [
                'available_ingredient_list'=>Ingredient::getAvailableIngredientList($update_dish_ingredients_list),
                'dish_model'=>$dish_model,
                'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($update_dish_ingredients_list),
                'message'=>$message
            ]);

        } else {

            $delete_results = Dish::checkDeleteIngredientFromDish($dish_model, $update_dish_ingredients_list);

            if (!$delete_results['success']) {
                return $this->redirect(['/dish/update/'.$id.'?message='.static::TOO_FEW_INGREDIENTS_FOR_UPDATE_DISH]);
            }

            $update_dish_ingredients_list = $delete_results['modified_dish_ingredients_list'];

            return $this->render('update', [
                'available_ingredient_list'=>Ingredient::getAvailableIngredientList($update_dish_ingredients_list),
                'dish_model'=>$dish_model,
                'manage_dish_ingredients_list'=>Ingredient::mergeIngredientNamesOnIds($update_dish_ingredients_list),
                'message'=>$message
            ]);
        }
    }


    public function actionDelete($id)
    {
        $this->findDishModel($id)->delete();
        return $this->redirect(['/admin?message='.static::DISH_SUCCESS_DELETE]);
    }


    public function actionSave()
    {
        return $this->redirect(Dish::saveNewDish());
    }


    private function findDishModel($id)
    {
        if (($dish_model = Dish::findOne($id)) !== null) {
            return $dish_model;
        } else {
            throw new NotFoundHttpException('Requested ingredient not exist');
        }
    }
}
