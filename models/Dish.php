<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\data\ActiveDataProvider;

class Dish extends \yii\db\ActiveRecord
{
    public $choice_ingredient;

    private const EXIST_FULL_OVERLAP = 'Есть полные совпадения ингредиентов';

    private const EXIST_PART_OVERLAP = 'Нет полных, но есть частичные совпадения ингредиентов';

    private const NO_NAME_IN_NEW_DISH = 'Ошибка создания блюда. Не заполнено название';

    private const TOO_FEW_INGREDIENTS_FOR_NEW_DISH = 'Ошибка создания блюда. Слишком мало ингредиентов';

    private const NEW_DISH_SUCCESS_ADD = 'Новое блюдо успешно добавлено';

    private const ERROR_ON_PROCESS_ADD = 'Возникла ошибка при сохранении. Пожалуйста проверьте данные и попробуйте снова';

    private const SIZE_DISH_LIST_PAGINATION = 10;

    private const MIN_QUANTITY_OF_INGREDIENTS = 2;

    public static function tableName()
    {
        return 'dish';
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT=>['name', 'choice_ingredient']
        ];
    }

    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id'=>'ingredient_id'])
                ->viaTable('dish_ingredient', ['dish_id'=>'id']);
    }

    public static function searchSuitable($search_params)
    {
        $list_ingredients_id = static::getIngredientsIdsOnNames($search_params);

        $dish_list = static::getSuitableDishList($list_ingredients_id);

        if (!count($dish_list)) {
            return null;
        }

        $full_dish_list = static::getFullDishList($dish_list);

        if (count($full_dish_list)) {
            return [
                'message'=>static::EXIST_FULL_OVERLAP,
                'dishes_list'=>$full_dish_list
            ];
        } else {
            return [
                'message'=>static::EXIST_PART_OVERLAP,
                'dishes_list'=>static::getPartiableDishList($dish_list)
            ];
        }
    }


    private static function getIngredientsIdsOnNames($search_names)
    {
        $list_ingredients_id = [];

        foreach ($search_names as $ingredient_name) {
            $search_id = static::getIngredientId(trim($ingredient_name));
            if (!empty($search_id)) {
                $list_ingredients_id[] = $search_id;
            }
        }

        return $list_ingredients_id;
    }


    private static function getSuitableDishList($list_ingredients_id)
    {
        $dish_suitable_list = (new Query())->select('di.dish_id, d.name, COUNT(di.ingredient_id) as quant')
            ->from('dish_ingredient di')
            ->where(['in', 'ingredient_id', $list_ingredients_id])
            ->andWhere(['not in', 'd.id',
                    (new Query())->select('dish_id')
                                          ->from('dish_ingredient')
                                          ->where(['in','ingredient_id',
                            (new Query())->select('id')
                                                 ->from('ingredient')
                                                 ->where(['=','hidden', true])
                             ])
                     ])
            ->groupBy(['di.dish_id'])
            ->having(['>', 'COUNT(di.ingredient_id)', 1])
            ->join('LEFT JOIN', 'dish d', 'd.id=di.dish_id')
            ->join('LEFT JOIN', 'ingredient i', 'i.id=di.ingredient_id')
            ->orderBy('quant DESC')
            ->all();

        return $dish_suitable_list;
    }


    private static function getFullDishList($dish_list)
    {
        $full_dish_list = [];

        for ($i=0; $i<count($dish_list); $i++) {

            $ingredients = (static::findOne($dish_list[$i]['dish_id']))->getIngredients()->all();

            if (count($ingredients) === (int)$dish_list[$i]['quant']) {

                $full_dish_list[] = [
                    'id'=>$dish_list[$i]['dish_id'],
                    'name'=>$dish_list[$i]['name'],
                    'ingredients'=>static::getIngredientStringList($ingredients)
                ];
            }
        }

        return $full_dish_list;
    }


    private static function getPartiableDishList($dish_list)
    {
        $partiable_dish_list = [];

        for ($i=0; $i<count($dish_list); $i++) {

            $ingredients = (static::findOne($dish_list[$i]['dish_id']))->getIngredients()->all();

            $partiable_dish_list[] = [
                'id'=>$dish_list[$i]['dish_id'],
                'name'=>$dish_list[$i]['name'],
                'ingredients'=>static::getIngredientStringList($ingredients)
            ];
        }

        return $partiable_dish_list;
    }


    private static function getIngredientStringList($ingredients_record)
    {
        $ingredients_set = [];

        foreach ($ingredients_record as $ingredient) {
            $ingredients_set[] = $ingredient->name;
        }

        return implode(', ', $ingredients_set);
    }


    private static function getIngredientId($name)
    {
        return (Ingredient::findByName($name)->id) ?? null;
    }


    public static function saveNewDish()
    {
        // Получение данных из сессии (проверка все ли норм)
        $dish_name = Yii::$app->session->get('new_dish_name') ?? null;
        $dish_ingredients = Yii::$app->session->get('manage_dish_ingredients') ?? [];

        if (empty($dish_name)) {
            return '/dish/create?message='.static::NO_NAME_IN_NEW_DISH;
        }

        if (count($dish_ingredients)<static::MIN_QUANTITY_OF_INGREDIENTS) {
            return '/dish/create?message='.static::TOO_FEW_INGREDIENTS_FOR_NEW_DISH;
        }

        static::transactionForAdd([
            'dish_name'=>$dish_name,
            'dish_ingredients'=>$dish_ingredients
        ]);

        static::sessionClearAfterAdd();

        return '/admin?message='.static::NEW_DISH_SUCCESS_ADD;
    }


    private static function transactionForAdd($action_params)
    {
        $new_dish = new Dish();

        $transaction = $new_dish->getDb()->beginTransaction();

            $new_dish->setAttribute('name', $action_params['dish_name']);
            $new_dish->save(false);

            foreach ($action_params['dish_ingredients'] as $ingredient) {

                $link_query = Yii::$app->db->createCommand('INSERT INTO dish_ingredient (id, dish_id, ingredient_id) VALUES (default, :dish_id, :ingredient_id)')
                                    ->bindValue(':dish_id', $new_dish->id)
                                    ->bindValue(':ingredient_id', $ingredient);

                if (!$link_query->execute()) {
                    $transaction->rollback();
                    return '/admin?message='.static::ERROR_ON_PROCESS_ADD;
                }
            }

        $transaction->commit();
    }


    private static function sessionClearAfterAdd()
    {
        Yii::$app->session->remove('manage_dish_ingredients');
        Yii::$app->session->remove('new_dish_name');
    }


    public function getIngredientsIds()
    {
        $ingredients_info_set = [];

        $ingredients_data = $this->getIngredients()->all();

        foreach ($ingredients_data as $ingredient) {
            $ingredients_info_set[] = $ingredient->id ?? null;
        }

        return $ingredients_info_set;
    }


    public static function getDishProvider()
    {
        $query_dish = Dish::find();

        return new ActiveDataProvider([
            'query' => $query_dish,
            'pagination' => [
                'pageSize' => static::SIZE_DISH_LIST_PAGINATION
            ]
        ]);
    }


    public static function addNewInfoOnCreateProcess($dish_model, $manage_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $manage_dish_ingredients_list;

        if (!empty($dish_model->choice_ingredient)) {
            if (!in_array($dish_model->choice_ingredient, $modified_dish_ingredients_list)) {
                $modified_dish_ingredients_list[] = $dish_model->choice_ingredient; //Ingredient::findByName($dish_model->choice_ingredient);
            }
        }
        Yii::$app->session->set('manage_dish_ingredients', $modified_dish_ingredients_list);
        Yii::$app->session->set('new_dish_name', $dish_model->name ?? "");

        return $modified_dish_ingredients_list;
    }


    public static function checkDiscardItemInIngredientList($manage_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $manage_dish_ingredients_list;

        if (!empty(Yii::$app->request->get()['discard'])) {

            $key = array_search(Yii::$app->request->get()['discard'], $modified_dish_ingredients_list);

            if ($key !== false) {
                unset($modified_dish_ingredients_list[$key]);
                Yii::$app->session->set('manage_dish_ingredients', $modified_dish_ingredients_list);
            }
        }

        return $modified_dish_ingredients_list;
    }



    public static function checkDeleteIngredientFromDish($dish_model, $update_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $update_dish_ingredients_list;

        if (!empty(Yii::$app->request->get()['discard'])) {

            $delete_ingredient_id = Yii::$app->request->get()['discard'];

            $key = array_search($delete_ingredient_id, $modified_dish_ingredients_list);
            if ($key !== false) {

                if (count($modified_dish_ingredients_list)>static::MIN_QUANTITY_OF_INGREDIENTS) {

                    Ingredient::deleteIngredientFromDish([
                        'dish_id'=>$dish_model->id,
                        'ingredient_id'=>$delete_ingredient_id,
                    ]);

                    unset($modified_dish_ingredients_list[$key]);

                    return [
                        'success'=>true,
                        'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
                    ];

                } else {

                    return [
                        'success'=>false,
                        'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
                    ];
                }
            }
        }

        return [
            'success'=>true,
            'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
        ];
    }


    public static function checkAddIngredientToDish($dish_model, $update_dish_ingredients_list)
    {
        $modified_dish_ingredients_list = $update_dish_ingredients_list;

        if (!empty($dish_model->name)) {

            $dish_model->save();

        } else {

            return [
                'success'=>false,
                'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
            ];
        }

        if (!empty($dish_model->choice_ingredient)) {
            Ingredient::addIngredientToDish([
                'dish_id'=>$dish_model->id,
                'ingredient_id'=>$dish_model->choice_ingredient
            ]);

            $modified_dish_ingredients_list[] = $dish_model->choice_ingredient;
        }

        return [
            'success'=>true,
            'modified_dish_ingredients_list'=>$modified_dish_ingredients_list
        ];
    }
}
