<?php

namespace app\models;

use Yii;

class SearchForm extends \yii\base\Model
{
    public $ingredient_one;
    public $ingredient_two;
    public $ingredient_three;
    public $ingredient_four;
    public $ingredient_five;

    private $dishes_find_result;
    private $message;

    private const MIN_QUANT_OF_INGREDIENTS = 2;
    private const LOW_QUANT_INGREDIENTS_MESSAGE = "Слишком мало ингредиентов";
    private const NO_DISH_FIND = 'Ничего не найдено';
    private const BASE_MESSAGE = 'Введите начальный список ингредиентов (не менее 2-х)';

    public function __construct()
    {
        $this->message = static::BASE_MESSAGE;
    }

    public function load($data, $formName = null)
    {
        $load_values = parent::load($data, $formName);

        $count_contents_fileds = 0;
        foreach ($this->getAttributes() as $field) {
            if ($field!=='') {
                $count_contents_fileds++;
            }
        }

        if ($count_contents_fileds < static::MIN_QUANT_OF_INGREDIENTS) {
            $this->message = static::LOW_QUANT_INGREDIENTS_MESSAGE;
            return false;
        }

        return $load_values;
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT=>[
                'ingredient_one',
                'ingredient_two',
                'ingredient_three',
                'ingredient_four',
                'ingredient_five'
            ]
        ];
    }

    public function findSuitableDishes()
    {
        if (($this->dishes_find_result = Dish::searchSuitable($this->getAttributes())) !== null) {

            $this->message = $this->dishes_find_result['message'];
            return true;
        }

        $this->message = static::NO_DISH_FIND;

        return false;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getDishesList()
    {
        return $this->dishes_find_result['dishes_list'];
    }
}
