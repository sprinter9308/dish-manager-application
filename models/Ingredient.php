<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class Ingredient extends \yii\db\ActiveRecord
{
    private const SIZE_INGREDIENT_LIST_PAGINATION = 10;

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'hidden' => 'Скрытый ингредиент'
        ];
    }

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Введите название ингредиента'],
            ['name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Введите строку английскими буквами'],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT=>['name', 'hidden']
        ];
    }

    public static function tableName()
    {
        return 'ingredient';
    }

    public function getDishes()
    {
        return $this->hasMany(Dish::className(), ['id'=>'dish_id'])->viaTable('dish_ingredient', ['dish_id'=>'id']);
    }

    public static function findByName($name)
    {
        return static::find()->where(['=', 'name', $name])->one();
    }

    public static function getNameById($id)
    {
        $ingredient = static::find()->where(['=', 'id', $id])->one();
        return $ingredient->name ?? '';
    }

    public static function getIngredientProvider()
    {
        $query_ingredient = Ingredient::find();

        return new ActiveDataProvider([
            'query' => $query_ingredient,
            'pagination' => [
                'pageSize' => static::SIZE_INGREDIENT_LIST_PAGINATION
            ]
        ]);
    }

    public static function deleteIngredientFromDish($delete_params)
    {
        $delete_query = Yii::$app->db->createCommand('DELETE FROM dish_ingredient WHERE ingredient_id=:ingredient_id AND dish_id=:dish_id')
                                    ->bindValue(':dish_id', $delete_params['dish_id'])
                                    ->bindValue(':ingredient_id', $delete_params['ingredient_id']);

        if ($delete_query->execute()) {
            return true;
        }
        return false;
    }


    public static function addIngredientToDish($add_params)
    {
        $add_query = Yii::$app->db->createCommand('INSERT INTO dish_ingredient (id, dish_id, ingredient_id) VALUES (default, :dish_id, :ingredient_id)')
                                    ->bindValue(':dish_id', $add_params['dish_id'])
                                    ->bindValue(':ingredient_id', $add_params['ingredient_id']);

        if ($add_query->execute()) {
            return true;
        }
        return false;
    }

    public static function mergeIngredientNamesOnIds($ingredients_id_set)
    {
        $merged_set = [];

        foreach ($ingredients_id_set as $ingredient) {
            $merged_set[]=[
                'index'=>$ingredient,
                'name'=> static::getNameById($ingredient)
            ];
        }

        return $merged_set;
    }

    public static function getAvailableIngredientList($manage_dish_ingredients_list)
    {
        $available_ingredient_set = static::find()->where(['not in', 'id', $manage_dish_ingredients_list])->all();
        return ArrayHelper::map($available_ingredient_set, 'id', 'name');
    }
}
